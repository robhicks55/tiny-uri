export const uriRegEx = /^(([^:\/?#]+):)?(\/\/([^\/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?/;
export const authRegEx = /^(.+\:.+@.+)/;
export const portRegEx = /:(\d+)$/;
export const numPortRegEx = /(\d+)$/;
export const qRegEx = /^([^=]+)(?:=(.*))?$/;
export const urlTempQueryRegEx = /\{\?(.*?)\}/;
